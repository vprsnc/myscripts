#!/usr/bin/env sh

eval "export $(egrep -z DBUS_SESSION_BUS_ADDRESS /proc/$(pgrep xmonad)/environ)";


mydate=`date`
myname="`whoami`@$HOST"
cd $HOME/org
git add .
git commit -m "$myname at: $mydate"
git push -u origin main && notify-send "Org synced" -a "myScripts" -u low
